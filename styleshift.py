# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 10:10:35 2018

@author: enovi
"""
from nltk.corpus import wordnet
import random

def main():
    choice = True
    source_text = ''
    target_text = ''
    c = 'y'
    print('Welcome to Style Shift!')
    while choice == True:
         c = input('Shift the style of an article y/n? ')
         if c == 'y':
             print('Enter the message with the preferred style.')
             source_text = get_text()
             print('Enter the message that will have its style shifted.')
             target_text = get_text()             
             shift_text(source_text,target_text)
             choice = True
         elif c == 'n':
             choice = False

def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def shift_text(source_text,target_text):
    source_text  = source_text.lower().split()
    target_text  = target_text.lower().split()
    synlist      = []
    synlists     = []
    shiftlist    = []
    shifted_text = []
    i            = 0
    j            = 0
    for word in target_text:
        
        synlists = wordnet.synsets(word)
        
        if len(synlists) > 0:
            i = random.randint(0, len(synlists) - 1)
            synlist = wordnet.synsets(word)[i].lemma_names()
            
            for item in source_text:
                
                if item in synlist:
                    shiftlist.append(item)
                
            if len(shiftlist) > 0:        
                j = random.randint(0, len(shiftlist) - 1)
                shifted_text.append(shiftlist[j])
            else:
                shifted_text.append(word)
                
            shiftlist = []
            synlist = []
            
        else:
            shifted_text.append(word)      
        
    print('Here is the shifted text:')       
    print(' '.join(shifted_text).capitalize())    
    
main()         
